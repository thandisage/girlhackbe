import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Navigation from '../Navigation/nav';
import * as ROUTES from '../../Constants/routes';
import './App.css';

class App extends Component {
  render(){
    return(
      <Router>
        <div>
          <Navigation/>
        </div>
        <hr/>

        {/* <Route path={ROUTES.HomePage} Component={HomePage}/> */}
      </Router>
    )
  }
};

export default App;
