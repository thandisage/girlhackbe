import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as ROUTES from '../../Constants/routes';
import './nav.css';

class Nav extends Component {
    render(){
        return(
            <div>
                <Link className={'homeURL'} to={ROUTES.HomePage}> HomePage </Link>
                <Link className={'Navigate'} to={ROUTES.Navigation}> Navigate </Link>
            </div>
        )
    }
};

export default Nav;
